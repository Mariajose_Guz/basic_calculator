class Calculator
  attr_accessor :x, :y, :op, :temp

  def initialize
  end 

  def operator(x,op,y)
    @x = x.to_i
    @op = op
    @y = y.to_i
    
    case @op
        when "+" then print @temp = @x + @y
        when "-" then print @temp = @x - @y
        when "x" then print @temp = @x * @y
        when "/" then print @temp = @x / @y
        else
        puts "Invalid operator"
    end
  end
end
    
calc = Calculator.new
calc.operator(ARGV[0],ARGV[1],ARGV[2])
