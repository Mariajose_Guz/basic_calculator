Feature: Basic Calculator

  Scenario Outline: Two numbers
    Given My numbers are <x> and <y>
    When My operation is "<op>"
    Then my result is "<temp>"

    Examples:
      | x       | op     | y      | temp   |
      | 5       | +      | 5      | 10     |
      | 25      | +      | 25     | 50     |
      | 50      | +      | 50     | 100    |
      | 100     | +      | 100    | 200    |
      | 5       | /      | 5      | 1      |
      | 25      | /      | 5      | 5      |
      | 50      | /      | 5      | 10     |
      | 10      | /      | 5      | 2      |
      | 5       | x      | 5      | 25     |
      | 25      | x      | 25     | 625    |
      | 50      | x      | 50     | 2500   |
      | 10      | x      | 10     | 100    |
      | 5       | -      | 5      | 0      |
      | 25      | -      | 5      | 20     |
      | 50      | -      | 5      | 45     |
      | 100     | -      | 5      | 95     |

