Given /^My numbers are (\d+) and (\d+)$/ do |x1, y1|
  @x = x1
  @y = y1
end

When /^My operation is "([^"]*)"$/ do |op1|
  @op = op1
  @res = `ruby basic_calculator.rb #{@x} #{@op} #{@y}`
end

Then /^my result is "([^"]*)"$/ do |temp1|
  @res.should == temp1
end